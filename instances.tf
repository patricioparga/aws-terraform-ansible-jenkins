#Get Linux AMI ID using SSM Parameter endpoint in us-east-1
data "aws_ssm_parameter" "linux-ami-controller" {
  provider = aws.region-controller
  name     = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}

#Get Linux AMI ID using SSM Parameter endpoint in us-west-2
data "aws_ssm_parameter" "linux-ami-worker" {
  provider = aws.region-worker
  name     = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}

#Create key-pair for logging into EC2 in us-east-1
resource "aws_key_pair" "key-controller" {
  provider   = aws.region-controller
  key_name   = "jenkins-key-controller"
  public_key = file("${var.jenkins_key_controller}.pub")
}

#Create key-pair for logging into EC2 in us-west-2
resource "aws_key_pair" "key-worker" {
  provider   = aws.region-worker
  key_name   = "jenkins-key-worker"
  public_key = file("${var.jenkins_key_worker}.pub")
}

#Create EC2 instance in us-east-1
resource "aws_instance" "jenkins-controller" {
  provider                    = aws.region-controller
  ami                         = data.aws_ssm_parameter.linux-ami-controller.value
  instance_type               = var.instance-type-controller
  key_name                    = aws_key_pair.key-controller.key_name
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.jenkins-sg.id]
  subnet_id                   = aws_subnet.subnet_controller_1.id

  tags = {
    Name = "jenkins_controller_tf"
  }

  depends_on = [aws_main_route_table_association.set-controller-default-rt-association]

  provisioner "local-exec" {
    command = <<EOF
aws --profile ${var.default-profile} ec2 wait instance-status-ok --region ${var.region-controller} --instance-ids ${self.id}
ansible-playbook --extra-vars 'passed_in_hosts=tag_Name_${self.tags.Name}' --key-file ${var.jenkins_key_controller} ansible_templates/jenkins-controller.yml
EOF
  }
  provisioner "local-exec" {
    command = "scp -i ${var.jenkins_key_controller} ec2-user@${self.public_ip}:/tmp/jenkins_password ./password.txt"
  }
}

data "local_file" "password" {
  depends_on = [aws_instance.jenkins-controller]
  filename   = "${path.module}/password.txt"
}


#Create EC2 instance in us-west-2
resource "aws_instance" "jenkins-worker" {
  provider                    = aws.region-worker
  count                       = var.workers-count
  ami                         = data.aws_ssm_parameter.linux-ami-worker.value
  instance_type               = var.instance-type-worker
  key_name                    = aws_key_pair.key-worker.key_name
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.jenkins-sg-worker.id]

  #Choose subnet based on availability zone
  subnet_id = element([aws_subnet.subnet_worker_1.id, aws_subnet.subnet_worker_2.id], count.index % 2)
  #Choose availability zone between a,b,c
  availability_zone = element(var.jenkins-worker-azs, count.index % 2)


  tags = {
    Name = join("_", ["jenkins_worker_tf", count.index + 1])
  }

  depends_on = [
    aws_main_route_table_association.set-worker-default-rt-association,
    aws_instance.jenkins-controller
  ]

  provisioner "local-exec" {
    command = <<EOF
aws --profile ${var.default-profile} ec2 wait instance-status-ok --region ${var.region-worker} --instance-ids ${self.id}
ansible-playbook --extra-vars 'passed_in_hosts=tag_Name_${self.tags.Name} controller_ip=${aws_instance.jenkins-controller.private_ip}' --key-file ${var.jenkins_key_worker} ansible_templates/jenkins-worker.yml
EOF
  }


}

  