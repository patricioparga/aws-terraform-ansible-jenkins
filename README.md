# AWS Terraform Jenkins
A jenkins server with two worker nodes deployed on AWS, using terraform for IaC

## Instalation steps
make sure to generate public and private keys for the controller and worker nodes
```
ssh-keygen -f ./ssh-keys/jenkins-key-controller
ssh-keygen -f ./ssh-keys/jenkins-key-worker
```

Apply configuration with terraform
```
terraform apply
```

Once all instances have been generated, the jenkins admins password will be printend on the console and stored on a local "password.txt" file. Use this to configure the controller and link the worker nodes.

You now have a fully functional jenkins server wit two worker nodes on AWS, managed through Terraform and Ansible. Congratulations! 🎊