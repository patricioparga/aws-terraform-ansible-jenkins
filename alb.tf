resource "aws_lb" "application-lb" {
  provider           = aws.region-controller
  name               = "jenkins-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb-sg.id]
  subnets            = [aws_subnet.subnet_controller_1.id, aws_subnet.subnet_controller_2.id]
  tags = {
    Name = "jenkins-lb"
  }
}

resource "aws_lb_target_group" "app-lb-tg" {
  provider    = aws.region-controller
  name        = "jenkins-app-lb-tg"
  port        = var.webserver-port
  target_type = "instance"
  vpc_id      = aws_vpc.vpc_controller.id
  protocol    = "HTTP"
  health_check {
    enabled  = true
    interval = 30
    path     = "/"
    port     = var.webserver-port
    protocol = "HTTP"
    matcher  = "200-299"
  }
  tags = {
    Name = "jenkins-app-target-group"
  }
}

resource "aws_lb_listener" "jenkins-listener-http" {
  provider          = aws.region-controller
  load_balancer_arn = aws_lb.application-lb.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app-lb-tg.arn
  }
}

resource "aws_lb_target_group_attachment" "jenkins-tg-attachment" {
  provider         = aws.region-controller
  target_group_arn = aws_lb_target_group.app-lb-tg.arn
  target_id        = aws_instance.jenkins-controller.id
  port             = var.webserver-port
}