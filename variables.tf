variable "default-profile" {
  description = "AWS profile to use"
  type        = string
  default     = "default"
}

variable "region-controller" {
  description = "AWS region to use"
  type        = string
  default     = "us-east-1"
}

variable "region-worker" {
  description = "AWS region to use"
  type        = string
  default     = "us-west-2"
}

variable "external_ip" {
  description = "External IP to allow SSH access"
  type        = string
  default     = "0.0.0.0/0"
}

variable "jenkins_key_controller" {
  description = "path to controller ssh key"
  type        = string
  default     = "./ssh-keys/jenkins-key-controller"
}

variable "jenkins_key_worker" {
  description = "path to worker ssh key"
  type        = string
  default     = "./ssh-keys/jenkins-key-worker"
}

variable "workers-count" {
  description = "Number of workers to create"
  type        = number
  default     = 2
}

variable "instance-type-controller" {
  description = "Instance type for controller"
  type        = string
  default     = "t2.micro"
}

variable "instance-type-worker" {
  description = "Instance type for worker"
  type        = string
  default     = "t2.micro"
}

variable "jenkins-worker-azs" {
  description = "Availability zones for worker"
  type        = list(string)
  default     = ["us-west-2a", "us-west-2b"]
}

variable "webserver-port" {
  description = "Port for apache webserver"
  type        = number
  default     = 8080
}
