output "jenkins-controller-public-ip" {
  value = aws_instance.jenkins-controller.public_ip
}

output "jenkins-controller-private-ip" {
  value = aws_instance.jenkins-controller.private_ip
}

output "jenkins-worker-public-ip" {
  value = {
    for instance in aws_instance.jenkins-worker :
    instance.id => instance.public_ip
  }
}

output "jenkins-worker-private-ip" {
  value = {
    for instance in aws_instance.jenkins-worker :
    instance.id => instance.private_ip
  }
}

output "jenkins-lb-dns" {
  value = aws_lb.application-lb.dns_name
}

output "jenkins_password" {
  value = data.local_file.password.content
}