provider "aws" {
  profile = var.default-profile
  region  = var.region-controller
  alias   = "region-controller"
}

provider "aws" {
  profile = var.default-profile
  region  = var.region-worker
  alias   = "region-worker"
}
