#Create VPC in us-east-1
resource "aws_vpc" "vpc_controller" {
  provider             = aws.region-controller
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "controller-vpc-jenkins"
  }
}

#Create VPC in us-west-2
resource "aws_vpc" "vpc_worker" {
  provider             = aws.region-worker
  cidr_block           = "192.168.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "worker-vpc-jenkins"
  }
}


#Create IGW in us-east-1
resource "aws_internet_gateway" "igw_controller" {
  provider = aws.region-controller
  vpc_id   = aws_vpc.vpc_controller.id
  tags = {
    Name = "controller-igw-jenkins"
  }
}

#Create IGW in us-west-2
resource "aws_internet_gateway" "igw_worker" {
  provider = aws.region-worker
  vpc_id   = aws_vpc.vpc_worker.id
  tags = {
    Name = "worker-igw-jenkins"
  }
}

#Get all availability zones in vpc for controller region
data "aws_availability_zones" "available_azs_controller" {
  provider = aws.region-controller
  state    = "available"
}

#Create subnet #1 in us-east-1
resource "aws_subnet" "subnet_controller_1" {
  provider          = aws.region-controller
  availability_zone = element(data.aws_availability_zones.available_azs_controller.names, 0)
  vpc_id            = aws_vpc.vpc_controller.id
  cidr_block        = "10.0.1.0/24"
}

# Create subnet #2 in us-east-1
resource "aws_subnet" "subnet_controller_2" {
  provider          = aws.region-controller
  availability_zone = element(data.aws_availability_zones.available_azs_controller.names, 1)
  vpc_id            = aws_vpc.vpc_controller.id
  cidr_block        = "10.0.2.0/24"
  tags = {
    Name = "subnet-controller-1"
  }
}

#Create subnet #1 in us-west-2
resource "aws_subnet" "subnet_worker_1" {
  provider          = aws.region-worker
  availability_zone = element(var.jenkins-worker-azs, 0)
  vpc_id            = aws_vpc.vpc_worker.id
  cidr_block        = "192.168.1.0/24"
  tags = {
    Name = "subnet-worker-1"
  }
}

#Create subnet #2 in us-west-2
resource "aws_subnet" "subnet_worker_2" {
  provider          = aws.region-worker
  availability_zone = element(var.jenkins-worker-azs, 1)
  vpc_id            = aws_vpc.vpc_worker.id
  cidr_block        = "192.168.2.0/24"
  tags = {
    Name = "subnet-worker-2"
  }
}

#Initiate Peering connection request from us-east-1
resource "aws_vpc_peering_connection" "useast1-uswest2" {
  provider    = aws.region-controller
  peer_vpc_id = aws_vpc.vpc_worker.id
  vpc_id      = aws_vpc.vpc_controller.id
  peer_region = var.region-worker
  tags = {
    Name = "vpc-peering-jenkins"
  }
}

#Accept VPC peering request in us-west-2 from us-east-1
resource "aws_vpc_peering_connection_accepter" "accept_peering" {
  provider                  = aws.region-worker
  vpc_peering_connection_id = aws_vpc_peering_connection.useast1-uswest2.id
  auto_accept               = true
  tags = {
    Name = "vpc-peering-jenkins-accepter"
  }
}

#Create route table in us-east-1
resource "aws_route_table" "internet_route" {
  provider = aws.region-controller
  vpc_id   = aws_vpc.vpc_controller.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw_controller.id
  }
  route {
    cidr_block                = "192.168.1.0/24"
    vpc_peering_connection_id = aws_vpc_peering_connection.useast1-uswest2.id
  }
  route {
    cidr_block                = "192.168.2.0/24"
    vpc_peering_connection_id = aws_vpc_peering_connection.useast1-uswest2.id
  }
  lifecycle {
    ignore_changes = all
  }
  tags = {
    Name = "controller-route-table-jenkins"
  }
}

#Overwrite default route trable of VPC(Controller) with our route table entries
resource "aws_main_route_table_association" "set-controller-default-rt-association" {
  provider       = aws.region-controller
  vpc_id         = aws_vpc.vpc_controller.id
  route_table_id = aws_route_table.internet_route.id
}

#Create route table in us-west-2
resource "aws_route_table" "internet_route_worker" {
  provider = aws.region-worker
  vpc_id   = aws_vpc.vpc_worker.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw_worker.id
  }
  route {
    cidr_block                = "10.0.1.0/24"
    vpc_peering_connection_id = aws_vpc_peering_connection.useast1-uswest2.id
  }
  route {
    cidr_block                = "10.0.2.0/24"
    vpc_peering_connection_id = aws_vpc_peering_connection.useast1-uswest2.id
  }
  lifecycle {
    ignore_changes = all
  }
  tags = {
    Name = "worker-route-table-jenkins"
  }
}

#Overwrite default route trable of VPC(worker) with our route table entries
resource "aws_main_route_table_association" "set-worker-default-rt-association" {
  provider       = aws.region-worker
  vpc_id         = aws_vpc.vpc_worker.id
  route_table_id = aws_route_table.internet_route_worker.id
}
