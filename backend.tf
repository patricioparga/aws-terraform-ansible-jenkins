terraform {
  required_version = ">=1.8.0"
  backend "s3" {
    region  = "us-east-1"
    profile = "default"
    key     = "terraformstatefile"
    bucket  = "maple-general-bucket"
  }
}
